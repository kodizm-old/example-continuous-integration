<?php

class VIRTest extends \PHPUnit\Framework\TestCase {

    /** @test */
    public function it_exists() {
        $this->assertTrue(function_exists('vir'));
    }

    /** @test */
    public function it_can_calculate() {
        $this->assertEquals(15, vir(3, 5));
    }
}