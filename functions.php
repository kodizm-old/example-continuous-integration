<?php

/**
 * The calculate VIR
 *
 * @param int $i
 * @param int $r
 * @return int
 */
function vir($i, $r) {
    return $i * $r;
}